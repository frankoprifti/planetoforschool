package com.planeto;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_TEXT = "com.planeto.EXTRA_TEXT";
    public static final String EXTRA_NUMBER = "com.planeto.EXTRA_NUMBER";

    ListView listView;
    String mTitle[] = {"Merkurri", "Aferdita", "Toka", "Marsi", "Jupiteri","Saturni","Urani","Neptuni","Plutoni"};
    String mDescription[] = {"91,691,000", "41,400,000", "0", "78,340,000", "628,730,000", "1,275,000,000", "2,723,950,000", "4,351,400,000", "5,044,300,000"};
    int images[] = {R.drawable.mercury, R.drawable.venus, R.drawable.earth, R.drawable.mars, R.drawable.jupiter,R.drawable.saturn,R.drawable.uranus,R.drawable.neptune,R.drawable.pluto};
    // so our images and other things are set in array

    // now paste some images in drawable

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);
        // now create an adapter class

        MyAdapter adapter = new MyAdapter(this, mTitle, mDescription, images);
        listView.setAdapter(adapter);
        // there is my mistake...
        // now again check this..

        // now set item click on list view
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, mDescription[position], Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(MainActivity.this, PlanetActivity.class);
                intent.putExtra(EXTRA_NUMBER,position);
                startActivity(intent);
            }
        });
        // so item click is done now check list view
    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String rTitle[];
        String rDescription[];
        int rImgs[];

        MyAdapter (Context c, String title[], String description[], int imgs[]) {
            super(c, R.layout.row, R.id.textView1, title);
            this.context = c;
            this.rTitle = title;
            this.rDescription =description;
            this.rImgs = imgs;

        }

        @Override
        public View getView(int position, View convertView,  ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myTitle = row.findViewById(R.id.textView1);
            TextView myDescription = row.findViewById(R.id.textView2);

            // now set our resources on views
            images.setImageResource(rImgs[position]);
            myTitle.setText(rTitle[position]);
            if(position==2){
                myDescription.setText("Ne jemi ketu");
            }else{
                myDescription.setText( "Distanca nga Toka: "+rDescription[position]);
            }





            return row;
        }
    }
}

